export function init() {

    document.addEventListener("DOMContentLoaded", function () {

        priorityNav.init({
            initClass: "has-longtail",              // Class that will be printed on html element to allow conditional css styling.
            mainNavWrapper: ".menu--longtail",                 // mainnav wrapper selector (must be direct parent from mainNav)
            mainNav: ".menu__items",          // mainnav selector. (must be inline-block)
            navDropdownClassName: "dropdown",        // class used for the dropdown - this is a class name, not a selector.
            navDropdownToggleClassName: "menu__tail", // class used for the dropdown toggle - this is a class name, not a selector.

            navDropdownLabel: "•••",       // Text that is used for the dropdown toggle.
            navDropdownBreakpointLabel: "•••",       //button label for navDropdownToggle when the breakPoint is reached.
            breakPoint: 380,         //amount of pixels when all menu items should be moved to dropdown to simulate a mobile menu
            throttleDelay: 50,          // this will throttle the calculating logic on resize because i'm a responsible dev.
            offsetPixels: 50,          // increase to decrease the time it takes to move an item.
            count: true,        // prints the amount of items are moved to the attribute data-count to style with css counter.

            moved: () => {
            },

            movedBack: () => {
            },
        });


        for (let longtailToggle of document.getElementsByClassName('menu__tail')) {

            longtailToggle.addEventListener('click', () => {

                let drop = longtailToggle.parentNode.querySelector('.dropdown')
                let pos = longtailToggle.getBoundingClientRect()

                if (pos.top > window.innerHeight / 2) {
                    drop.style.bottom = longtailToggle.parentNode.clientHeight + "px"
                }

                if (pos.left > window.innerWidth / 2) {
                    drop.style.right = 0
                }
            })
        }
    });
}