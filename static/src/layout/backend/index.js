import './index.sass';

document.addEventListener("DOMContentLoaded", function () {

    for (let rep of getIconReplacements()) {

        for (let icon of document.querySelectorAll(rep.from)) {
            icon.className = '';
            icon.dataset.feather = rep.to;
        }
    }

    setTimeout(() => {
        feather.replace();
    }, 500)
});

function getIconReplacements() {
    return [
        {from: '.fa-bug', to: 'crosshair'}
    ]
}