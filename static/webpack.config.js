const path = require('path');
const webpack = require('webpack');
const LiveReloadPlugin = require('webpack-livereload-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// Is the current build a development build
const IS_DEV = (process.env.NODE_ENV === 'dev');

const dirNode = 'node_modules';
const dirSrc = path.join(__dirname, 'src');
const dirAssets = path.join(__dirname, 'assets');


/**
 * Webpack Configuration
 */
module.exports = {
    entry: {
        'layout/backend': path.join(dirSrc, 'layout/backend/index.js'),
        'apps/mail': path.join(dirSrc, 'apps/mail/index.js'),
        'albert/albert': path.join(dirSrc, 'albert/build.js'),
    },
    output: {
        filename: '[name].js',
        path: dirAssets
    },
    resolve: {
        modules: [
            dirNode,
            dirSrc
        ]
    },
    externals: {
        jquery: 'jQuery'
    },
    plugins: [
        new webpack.DefinePlugin({
            IS_DEV: IS_DEV,
        }),
        new LiveReloadPlugin(),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "[name].css",
            chunkFilename: "[id].css"
        })
    ],
    module: {
        rules: [
            {test: /\.js$/, loader: 'babel-loader', exclude: /(node_modules)/, options: {compact: true}},
            {test: /\.css$/, use: ['style-loader', {loader: 'css-loader', options: {sourceMap: IS_DEV}}]},
            {
                test: /\.s(a|c)ss/, use: ['style-loader',
                {loader: MiniCssExtractPlugin.loader, options: {publicPath: dirAssets}},
                {loader: 'css-loader', options: {sourceMap: IS_DEV}},
                {loader: 'sass-loader', options: {sourceMap: IS_DEV, includePaths: [dirSrc]}}]
            },
            {test: /\.(jpe?g|png|gif)$/, loader: 'file-loader', options: {name: '[path][name].[ext]'}}
        ]
    }
};
