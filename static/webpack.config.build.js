const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpackConfig = require('./webpack.config');

module.exports = merge(webpackConfig, {

    devtool: 'source-map',

    plugins: [
        new CleanWebpackPlugin(['assets']),
        new CopyWebpackPlugin([
            { from: './node_modules/feather-icons/dist', to: './vendor/feather' },
            { from: './node_modules/priority-nav/dist', to: './vendor/priority-nav' },
            { from: './node_modules/okaynav/dist', to: './vendor/okaynav' },
            { from: './src/font', to: './font' },
            ])
    ]

});
