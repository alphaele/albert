# -*- coding: utf-8 -*-
# Copyright (C) 2018 Alphaele
# License CC BY-NC-ND 4.0
{
    "name": "Albert theme",
    "version": "12.0.0.1",
    "author": "Alphaele",
    "license": "Other proprietary",
    "category": "Theme/Backend",
    "depends": [
    ],
    "data": [
        "views/layout/backend/assets.xml",
        "views/layout/backend/header.xml",
    ],
}
